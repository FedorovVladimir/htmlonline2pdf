package decorator;

import builder.CompositeItem;
import document.Item;
import document.Tags;

public class BoldDecorator extends CompositeItem {
    public BoldDecorator(Item item) {
        super(Tags.get("b"));
        add(item);
    }
}
