package gui;

import document.Item;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;


public class ActionText implements ChangeListener {

    private Item item;

    public ActionText(Item item) {
        this.item = item;
    }

    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
        item.setValue((String) newValue);
    }
}
