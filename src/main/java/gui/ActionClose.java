package gui;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class ActionClose implements EventHandler {

    private Stage newWindow;

    public ActionClose(Stage newWindow) {
        this.newWindow = newWindow;
    }

    public void handle(Event event) {
        newWindow.close();
    }
}
