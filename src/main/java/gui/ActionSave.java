package gui;

import adapter.FileSystem;
import adapter.Save;
import document.Item;
import javafx.event.Event;
import javafx.event.EventHandler;

import java.io.IOException;

public class ActionSave implements EventHandler {

    private Item html;

    public ActionSave(Item html) {
        this.html = html;
    }

    public void handle(Event event) {
        System.out.println(html.getHtml());
        Save save = new FileSystem();
        try {
            save.save(html, "src\\main\\resources\\Ваш договор.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
