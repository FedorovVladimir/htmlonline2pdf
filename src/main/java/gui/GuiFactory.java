package gui;

import adapter.FileSystem;
import adapter.Save;
import chane_of_next.BaseHandler;
import chane_of_next.Handler;
import document.Item;
import document.WebEngineListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class GuiFactory {

    private static GuiFactory guiFactory = new GuiFactory();

    private double size = 3;

    private int leftPadding = 20;
    private int rightPadding = 20;
    private int topPadding = 5;
    private int bottomPadding = 5;

    private double width = 210*size;
    private double height = 297*size;

    private double widthForm = 210*size + leftPadding + rightPadding + 12;
    private double heightForm = 297*size + topPadding + bottomPadding + 50 + 50;

    private Item html;
    private static Stage stage;

    private final WebView browser = createWebView();
    private final WebEngine engine = browser.getEngine();
    private WebEngineListener webEngineListener;

    private static Stage newWindow;
    private VBox root = createRoot(browser);

    public void init(Item html, Stage stage) {
        this.html = html;
        this.stage = stage;
        webEngineListener = new WebEngineListener(engine, html);
        this.html.subscribe(webEngineListener);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        String title = "DocumentMaker";
        stage.setTitle(title);
        stage.setWidth(widthForm);
        stage.setHeight(heightForm);
        stage.setX(50);
        stage.setY(50);

        Button button = new Button("Сохранить");
        Button buttonStart = new Button("Начать заполнение");
        root.getChildren().addAll(button, buttonStart);
        button.setOnAction(new ActionSave(html));
        buttonStart.setOnAction(new ActionStart(html));
    }

    private VBox createRoot(WebView browser) {
        VBox root = new VBox();
        root.getChildren().add(browser);
        root.setPadding(new Insets(topPadding, rightPadding , bottomPadding, leftPadding));
        return root;
    }

    private WebView createWebView() {
        WebView browser = new WebView();
        browser.setMinSize(width, height);
        browser.setMaxSize(width, height);
        return browser;
    }

    public static TextField createInput(String name, String text, int i) {
        Label secondLabel = new Label(text);
        secondLabel.setLayoutX(10);
        secondLabel.setLayoutY(12);
        AnchorPane secondaryLayout = new AnchorPane();
        secondaryLayout.getChildren().add(secondLabel);
        Scene secondScene = new Scene(secondaryLayout, 240, 120);
        newWindow = new Stage();
        newWindow.setX(i % 3 * 260 + 800);
        newWindow.setY(i / 3 * 260 + 50);
        newWindow.setTitle(name);
        newWindow.setScene(secondScene);
        newWindow.initModality(Modality.WINDOW_MODAL);
        newWindow.initOwner(stage);
        TextField textField = new TextField();
        textField.setLayoutX(10);
        textField.setLayoutY(40);
        secondaryLayout.getChildren().add(textField);
        newWindow.show();
        Button button = new Button("Далее");
        secondaryLayout.getChildren().add(button);
        button.setLayoutX(10);
        button.setLayoutY(80);
        ActionClose actionClose = new ActionClose(newWindow);
        button.setOnAction(actionClose);
        return textField;
    }

    private GuiFactory() {

    }

    public static GuiFactory getInstance() {
        return guiFactory;
    }
}
