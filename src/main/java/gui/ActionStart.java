package gui;

import builder.Builder;
import chane_of_next.BaseHandler;
import chane_of_next.Handler;
import document.Item;
import javafx.event.Event;
import javafx.event.EventHandler;

public class ActionStart implements EventHandler {

    private Builder builder = new Builder();
    private Item html;
    private Handler handler;

    Item name = builder.makeP("");
    Item old = builder.makeP("");
    Item mass = builder.makeP("");

    public ActionStart(Item html) {
        this.html = html;
        html.add(builder.makeP("Здравствуйте ")).add(name);
        html.add(builder.makeP("Ваш возраст ")).add(old);
        html.add(builder.makeP("Ваш вес ")).add(mass);
        handler = new BaseHandler("Имя", "Как вас зовут?", name);
        Handler handler2 = new BaseHandler("Возраст", "Сколько вам лет?", old);
        Handler handler3 = new BaseHandler("Все", "Сколько вы весите?", mass);
        handler.add(handler2);
        handler2.add(handler3);
    }

    public void handle(Event event) {
        handler.handle(0);
    }
}
