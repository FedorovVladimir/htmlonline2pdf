package document;

import java.util.LinkedList;
import java.util.List;

public abstract class Item {

    protected String tag;
    protected Item parent;
    protected List<Item> list = new LinkedList<Item>();
    private List<Listener> listeners = new LinkedList<Listener>();

    public Item(String tag) {
        this.parent = this;
        this.tag = tag;
    }

    public void setParent(Item parent) {
        this.parent = parent;
    }

    public List<Listener> getListeners() {
        return listeners;
    }

    public abstract Item add(Item item);
    public abstract String getHtml();

    public void subscribe(Listener listener) {
        listeners.add(listener);
        for (Item i: list) {
            i.subscribe(listener);
        }
    }

    public void notifyListeners() {
        for (Listener listener: listeners) {
            listener.update();
        }
    }

    public abstract String getValue();

    public abstract void setValue(String newValue);
}
