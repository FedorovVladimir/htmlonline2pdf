package document;

import javafx.scene.web.WebEngine;

public class WebEngineListener implements Listener {

    private WebEngine webEngine;
    private Item html;

    public WebEngineListener(WebEngine webEngine, Item html) {
        this.webEngine = webEngine;
        this.html = html;
    }

    public void update() {
        webEngine.loadContent(html.getHtml());
    }
}
