package document;

public interface Listener {
    void update();
}
