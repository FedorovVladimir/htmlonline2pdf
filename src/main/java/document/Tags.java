package document;

import java.util.ArrayList;
import java.util.List;

public class Tags {

    private static List<String> tags = new ArrayList<String>();

    public static String get(String tag) {
        for (int i = 0; i < tags.size(); i++) {
            if (tags.get(i).equals(tag)) {
                return tags.get(i);
            }
        }
        tags.add(tag);
        return tags.get(tags.size() - 1);
    }
}
