package chane_of_next;

import document.Item;
import gui.ActionText;
import gui.GuiFactory;
import javafx.scene.control.TextField;

public class BaseHandler implements Handler {

    private Handler handler;
    private String name;
    private String text;
    private Item html;

    public BaseHandler(String name, String text, Item html) {
        this.name = name;
        this.text = text;
        this.html = html;
    }

    public void add(Handler handler) {
        this.handler = handler;
    }

    public void handle(int i) {
        TextField textField = GuiFactory.createInput(name, text, i);
        textField.setText(html.getValue());
        ActionText actionText = new ActionText(html);
        textField.textProperty().addListener(actionText);

        if (handler != null) {
            handler.handle(i + 1);
        }
    }
}
