package chane_of_next;

public interface Handler {
    void add(Handler handler);
    void handle(int i);
}
