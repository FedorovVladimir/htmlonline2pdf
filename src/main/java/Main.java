import builder.Builder;
import document.Item;
import gui.GuiFactory;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static Builder builder = new Builder();
    public static Item html = builder.makeHtml();

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) {
        GuiFactory.getInstance().init(html, stage);
        stage.show();


    }
}