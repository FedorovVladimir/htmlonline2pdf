package builder;

import document.Item;
import document.Tags;

public class Director {

    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public Item makeArticle(String header, String text) {
        Item item = new CompositeItem(Tags.get("div"));
        item.add(builder.makeH1(header));
        item.add(builder.makeP(text));
        return item;
    }
}
