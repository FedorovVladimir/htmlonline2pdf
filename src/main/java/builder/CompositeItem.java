package builder;

import document.Item;
import document.Listener;

public class CompositeItem extends Item {

    public CompositeItem(String tag) {
        super(tag);
    }

    public Item add(Item item) {
        item.setParent(this);
        for (Listener listener: parent.getListeners()) {
            item.subscribe(listener);
        }
        list.add(item);
        notifyListeners();
        return this;
    }

    public String getHtml() {
        StringBuilder codeBuilder = new StringBuilder("<" + tag + ">").append("\n");
        for (Item i: list) {
            codeBuilder.append(i.getHtml()).append("\n");
        }
        String code = codeBuilder.toString();
        code += "</" + tag + ">";
        return code;
    }

    public String getValue() {
        return "";
    }

    public void setValue(String newValue) {
        // TODO: 19.03.2019
    }
}
