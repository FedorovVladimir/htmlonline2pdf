package builder;

import document.Item;
import document.Tags;

public class Builder {
    public Item makeH1(String value) {
        return new LeafItem(Tags.get("h1"), value);
    }

    public Item makeP(String value) {
        return new LeafItem(Tags.get("p"), value);
    }

    public Item makeHtml() {
        return new CompositeItem(Tags.get("html"));
    }
}
