package builder;

import document.Item;
import document.Tags;

public class LeafItem extends Item {

    private String value;

    public LeafItem(String tag, String value) {
        super(tag);
        this.value = value;
    }

    public Item add(Item item) {
        return this;
    }

    public String getHtml() {
        return "<" + tag + ">" + value + "</" + tag + ">";
    }

    public String getValue() {
        return value;
    }

    public void setValue(String newValue) {
        this.value = newValue;
        notifyListeners();
    }
}
