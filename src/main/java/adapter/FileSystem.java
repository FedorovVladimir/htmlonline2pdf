package adapter;

import com.itextpdf.html2pdf.HtmlConverter;
import document.Item;

import java.io.FileOutputStream;
import java.io.IOException;


public class FileSystem implements Save {
    public void save(Item html, String dest) throws IOException {
        HtmlConverter.convertToPdf(html.getHtml(), new FileOutputStream(dest));
    }
}
