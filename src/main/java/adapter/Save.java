package adapter;

import document.Item;

import java.io.IOException;

public interface Save {
    void save(Item html, String dest) throws IOException;
}
